import React, { useState, Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PlayerScreen from './PlayerScreen';
import HomeScreen from './HomeScreen';
import { render } from 'react-dom';

const AppNavigator = createStackNavigator(
  {
    Login: HomeScreen,
    Player: PlayerScreen,
  },
  {
    initialRouteName: 'Login',
  }
);

const AppContainer = createAppContainer(AppNavigator)
//export default AppContainer;
//export default createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}