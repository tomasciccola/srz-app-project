import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    contentContainer: {
      paddingTop: 30,
    },
    welcomeContainer: {
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 20,
    },
    SRZ: {
      marginTop: 20,
      fontSize: 50,
      fontWeight: 'bold',
      color: '#303030'
    },
    getStartedContainer: {
      alignItems: 'center',
      marginHorizontal: 50,
    },
    getStartedText: {
      fontSize: 17,
      color: 'rgba(96,100,109, 1)',
      lineHeight: 24,
      textAlign: 'center',
    },
    button: {
      backgroundColor: '#6600FF',
      padding: 15,
      marginTop: 20,
    },
    buttonText: {
      
      color: '#fff',
      textAlign: 'center',
    }
  });

  
export default styles;