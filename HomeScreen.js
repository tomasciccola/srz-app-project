import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import styles from './Style.js';
import { Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const baseURL = 'http://srz.evennode.com/';

//export default function HomeScreen() {
export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      cliente: '',
      licencia: '',
    };
    this.signInHandler = this.signInHandler.bind(this);
  }

  // FUNCION DE SIGN IN AL TOCAR ENTRAR
  signInHandler = async () => {
    const { cliente, licencia } = this.state;

    var data = new FormData();
    data.append('cliente', cliente);
    data.append('licencia', licencia);

    const apiCall = await fetch(baseURL + this.state.cliente);
    const response = await apiCall.json();
      // .then(resp => {
      //   console.log(resp.json());
      //   return resp.json();
      // })
      // .catch(error => {
      //   console.log('Error de respuesta de API.');
      //   throw error;
      // });

    if (response == 'Error') {
      console.log(response);
      console.log('mal login.');
      await Alert.alert(this.state.cliente, this.state.licencia);
    } else {
      console.log('login valido');
      this.props.navigation.navigate('Player');
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Text style={styles.SRZ}>
              SRZ
          </Text>
          </View>

          <View style={styles.getStartedContainer}>
            <Text style={styles.getStartedText}>
              Bienvenidx
          </Text>
            <Text style={styles.getStartedText}>
              Ingrese Usuario y Licencia para comenzar.
          </Text>
          </View>

          <View style={{ marginTop: 20, marginRight: 25, marginLeft: 25 }}>
            <View>
              <Text>Cliente</Text>
              <TextInput style={{ marginBottom: 10, height: 40, borderColor: 'gray', borderWidth: 1, paddingLeft: 10 }}
                onChangeText={(cliente) => this.setState({ cliente })}
                value={this.state.cliente}
              />
            </View>
            <View>
              <Text>Licencia</Text>
              <TextInput style={{ marginBottom: 10, height: 40, borderColor: 'gray', borderWidth: 1, paddingLeft: 10 }}
                onChangeText={(licencia) => this.setState({ licencia })}
                value={this.state.licencia}
                secureTextEntry
              />
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => { this.signInHandler(); }} >
                <Text style={styles.buttonText} >Entrar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

// HomeScreen.navigationOptions = {
//   header: null,
// };
